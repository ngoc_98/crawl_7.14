import scrapy
import threading
import logging
from tutorial.items import CVEDetails
from twisted.internet.error import TimeoutError, TCPTimedOutError, ConnectionRefusedError
from twisted.web._newclient import ResponseFailed, ResponseNeverReceived
from scrapy.spidermiddlewares.httperror import HttpError
from scrapy.utils.response import response_status_message
from collections import defaultdict
from scrapy.utils.project import get_project_settings
from pymongo import MongoClient

class CveDetailsSpider(scrapy.Spider):
    name = 'cve_details'

    custom_settings = {
        'COLLECTION_NAME': 'cvedetails',
        'PRE_COLLECTION_NAME': 'cves',
    }

    def __init__(self, **kwargs):
        settings = get_project_settings()
        connection = MongoClient(
            settings['MONGODB_SERVER'],
            settings['MONGODB_PORT']
        )
        db = connection[settings['MONGODB_DB']]
        self.collection = db[self.custom_settings['PRE_COLLECTION_NAME']]
        self._ids = [field['_id'] for field in self.collection.find({}, {"_id": 1})]
        super().__init__(**kwargs)

    def start_requests(self):
        url = 'https://nvd.nist.gov/vuln/detail/'
        for i in range(int(self.inp1), int(self.inp2)):
            url_d = url + self._ids[i]
            yield scrapy.Request(url_d, callback=self.parse, meta={'id': self._ids[i]}, headers=self.get_headers_lv0(),
                                 cookies=self.get_cookies_lv0(), method="GET")

    def parse(self, response):
        item = CVEDetails()
        item['_id'] = response.meta['id']
        item['description'] = response.xpath('//p[@data-testid="vuln-description"]/text()').get()
        item['published_date'] = response.xpath('//span[@data-testid="vuln-published-on"]/text()').get()
        item['last_modified'] = response.xpath('//span[@data-testid="vuln-last-modified-on"]/text()').get()
        item['source'] = response.xpath('//span[@data-testid="vuln-current-description-source"]/text()').get()
        item['severity'] = {}
        item['severity']['cvss_ver_3x'] = {}
        item['severity']['cvss_ver_2'] = {}
        item['severity']['cvss_ver_3x']['base_score'] = response.xpath('//a[@id="Cvss3NistCalculatorAnchor"]/text()').get()
        item['severity']['cvss_ver_2']['base_score'] = response.xpath('//a[@id="Cvss2CalculatorAnchor"]/text()').get()
        yield scrapy.Request(url="https://www.vulnerabilitycenter.com/svc/svc/svc", callback=self.parse_lv2,
                             errback=self.errback, method="POST", headers=self.get_headers(), dont_filter=True,
                             body=self.get_body(item['_id']), cookies=self.get_cookies(), meta={'item': item})

    def parse_lv2(self, response):
        item = response.meta['item']

        try:
            item['skybox_id'] = response.text.split('","')[3]
            yield scrapy.Request(url="https://www.vulnerabilitycenter.com/svc/svc/svc", callback=self.parse_lv3,
                                 method='POST', errback=self.errback, dont_filter=True,
                                 headers=self.get_headers(), body=self.get_body_lv2(item['skybox_id']), cookies=self.get_cookies(),
                                 meta={'item': item})
        except:
            yield item

    def parse_lv3(self, response):
        item = response.meta['item']
        item.update(self.get_json(response.text))
        yield item

    def errback(self, failure):
        request = failure.request

        if failure.check(HttpError):
            response = failure.value.response
            self.logger.error(
                'errback <%s> %s , response status:%s' %
                (request.url, failure.value, response_status_message(response.status))
            )
        elif failure.check(ResponseFailed):
            self.logger.error('errback <%s> ResponseFailed' % request.url)

        elif failure.check(ConnectionRefusedError):
            self.logger.error('errback <%s> ConnectionRefusedError' % request.url)

        elif failure.check(ResponseNeverReceived):
            self.logger.error('errback <%s> ResponseNeverReceived' % request.url)

        elif failure.check(TCPTimedOutError, TimeoutError):
            self.logger.error('errback <%s> TimeoutError' % request.url)

        else:
            self.logger.error('errback <%s> OtherError' % request.url)

    @staticmethod
    def get_json(text):
        mydict = lambda: defaultdict(mydict)
        dict = mydict()
        text = text.replace('"],0,7]', '')
        text = text.split('","')
        dict["vendor"] = text[text.index("Vendor") + 1]
        dict["scanner"] = text[text.index("Scanners") + 1]
        dict["affected_products"]["product"] = text[text.index("Product") + 1]
        dict["affected_products"]["category"] = text[text.index("Category") + 1]
        dict["affected_products"]["affected_versions"] = text[text.index("Affected Versions") + 1]
        dict["solutions"] = []

        try:
            dict1 = {}
            dict1["name"] = text[text.index("name") + 1]
            dict1["type"] = text[text.index("type") + 1]
            dict1["description"] = text[text.index("description") + 1]
            dict["solutions"].append(dict1)
            dict2 = {}
            dict2["name"] = text[text.index("solution") + 2]
            dict2["type"] = text[text.index("solution") + 4]
            dict2["description"] = text[text.index("solution") + 3]
            dict["solutions"].append(dict2)
            start = text.index("solution") + 6
            if start == len(text) - 1:
                dict3 = {}
                dict3["name"] = text[text.index("solution") + 2]
                dict3["type"] = text[text.index("solution") + 4]
                dict3["description"] = text[text.index("solution") + 6]
                dict["solutions"].append(dict3)
            else:
                for i in range(start, len(text) - 1, 2):
                    dict3 = {}
                    dict3["name"] = text[text.index("solution") + 2]
                    dict3["type"] = text[text.index("solution") + 4]
                    dict3["description"] = text[i]
                    dict["solutions"].append(dict3)
        except:
            pass
        return dict

    @staticmethod
    def get_headers():
        headers = {
            "authority": "www.vulnerabilitycenter.com",
            "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\", \"Google Chrome\";v=\"90\"",
            "x-gwt-module-base": "https://www.vulnerabilitycenter.com/svc/svc/",
            "x-gwt-permutation": "938A4411868A979D515A5CE34BBE59F8",
            "sec-ch-ua-mobile": "?0",
            "content-type": "text/x-gwt-rpc; charset=UTF-8",
            "accept": "*/*",
            "origin": "https://www.vulnerabilitycenter.com",
            "sec-fetch-site": "same-origin",
            "sec-fetch-mode": "cors",
            "sec-fetch-dest": "empty",
            "referer": "https://www.vulnerabilitycenter.com/svc/SVC.html",
            "accept-language": "en-US,en;q=0.9"
        }
        return headers

    @staticmethod
    def get_body(id):
        body = "7|0|8|https://www.vulnerabilitycenter.com/svc/svc/|4EB3C5214EE901B9C18B44EBC7FF695D|" \
               "com.skybox.svc.shared.SVCService|search|java.lang.String/2004016611|com.skybox.svc.shared.Day/" \
               "485962538|Z|{}|1|2|3|4|4|5|6|6|7|8|0|0|0|"
        return body.format(id)

    @staticmethod
    def get_cookies():
        cookie_string = "JSESSIONID=013D93A42C13C8C394BB6E23AFD58AB0; SESS9ab004767f41bf8781a9a80db17b50c6=" \
                        "ioestvbm99op2bihm5rndso7i5; has_js=1; d-a8e6=7a57e4df-2d16-49b3-8505-c3c4a4fbfe4e; " \
                        "__utmc=66189737; _mkto_trk=id:440-MPQ-510&token:_mch-vulnerabilitycenter.com" \
                        "-1626233949740-15893; __adroll_fpc=8bee9b06fa2a2428e5b39d6ecafeaf52-1626233951918; " \
                        "_fbp=fb.1.1626233953278.1377671768; __utmz=66189737.1626361011.9.4.utmcsr=google|utmccn=" \
                        "(organic)|utmcmd=organic|utmctr=(not%20provided); OptanonAlertBoxClosed=2021-07-15T17:" \
                        "05:51.065Z; s-9da4=6ff63c2b-4672-4935-8f7e-bdc44ffde8c1; __utma=66189737.1200352611.1626233" \
                        "950.1626366486.1626374352.11; __ar_v4=A24BPKS7XRBSZPUWDMSPMD%3A20210713%3A41%7C4DUFBJ677FAQ" \
                        "NKPWAVW5TK%3A20210713%3A41%7CW5L5PZHOHNFQTHBFOIEKOW%3A20210713%3A41; OptanonConsent=landing" \
                        "Path=NotLandingPage&datestamp=Fri+Jul+16+2021+01%3A50%3A38+GMT%2B0700+(Indochina+Time)&" \
                        "version=3.6.28&groups=1%3A1%2C2%3A1%2C3%3A1%2C4%3A1%2C0_162101%3A1%2C0_162100%3A1%2C0_162" \
                        "099%3A1%2C0_162105%3A1%2C0_162104%3A1%2C0_162103%3A1%2C0_162102%3A1%2C0_162109%3A1%2C0_16" \
                        "2108%3A1%2C0_162107%3A1%2C0_162106%3A1%2C0_162113%3A1%2C0_162112%3A1%2C0_162111%3A1%2C0_162" \
                        "110%3A1&AwaitingReconsent=false; __utmt=1; __utmb=66189737.5.10.1626374352"
        cookies = {}
        for cookie in cookie_string.split('; '):
            try:
                key = cookie.split('=')[0]
                val = cookie.split('=')[1]
                cookies[key] = val

            except:
                pass
        return cookies

    @staticmethod
    def get_body_lv2(id):
        body = "7|0|6|https://www.vulnerabilitycenter.com/svc/svc/|4EB3C5214EE901B9C18B44EBC7FF695D|" \
               "com.skybox.svc.shared.SVCService|getVulnerabilityRecord|java.lang.String/2004016611|{}|1|2|3|4|1|5|6|"
        return body.format(id)

    @staticmethod
    def get_headers_lv0():
        headers = {
            "connection": "keep-alive",
            "pragma": "no-cache",
            "cache-control": "no-cache",
            "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\", \"Google Chrome\";v=\"90\"",
            "sec-ch-ua-mobile": "?0",
            "upgrade-insecure-requests": "1",
            "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
            "sec-fetch-site": "none",
            "sec-fetch-mode": "navigate",
            "sec-fetch-user": "?1",
            "sec-fetch-dest": "document",
            "accept-language": "en-US,en;q=0.9"
        }
        return headers

    @staticmethod
    def get_cookies_lv0():
        cookie_string = "__utmc=141729133; __utmz=141729133.1626232597.3.3.utmcsr=google|utmccn=(organic)|" \
                         "utmcmd=organic|utmctr=(not%20provided); _ga=GA1.2.2026449466.1626258140;" \
                         " __utma=141729133.1383366727.1626162764.1626751259.1626755450.25; __utmb=141729133.2.10.1626755450"
        cookies = {}
        for cookie in cookie_string.split('; '):
            try:
                key = cookie.split('=')[0]
                val = cookie.split('=')[1]
                cookies[key] = val

            except:
                pass
        return cookies