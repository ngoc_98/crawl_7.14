from pymongo import MongoClient
import scrapy
from tutorial.items import CVEDetails
from scrapy.utils.project import get_project_settings
from twisted.internet.error import TimeoutError, TCPTimedOutError, ConnectionRefusedError
from twisted.web._newclient import ResponseFailed, ResponseNeverReceived
from scrapy.spidermiddlewares.httperror import HttpError
from scrapy.utils.response import response_status_message
from collections import defaultdict

class SchedulerSpider(scrapy.Spider):
    name = 'scheduler'

    custom_settings = {
        'COLLECTION_NAME': 'cvedetails'
    }

    def __init__(self):
        settings = get_project_settings()
        connection = MongoClient(
            settings['MONGODB_SERVER'],
            settings['MONGODB_PORT']
        )
        db = connection[settings['MONGODB_DB']]
        self.collection = db[self.custom_settings['COLLECTION_NAME']]
        self.skybox_ids = [field['skybox_id'] for field in self.collection.find({"$or": [{"solutions": {"$exists": "false"}},
                                                                           {"solutions": {"$size": 0}}]}, {"skybox_id": 1})]

    def start_requests(self):
        for skybox_id in self.skybox_ids:
            yield scrapy.Request(url="https://www.vulnerabilitycenter.com/svc/svc/svc", callback=self.parse,
                                 method='POST', errback=self.errback, dont_filter=True,
                                 headers=self.get_headers(), body=self.get_body(skybox_id),
                                 cookies=self.get_cookies(),
                                 meta={'skybox_id': skybox_id})

    def parse(self, response):
        item = CVEDetails()
        item["skybox_id"] = response.meta['skybox_id']
        item.update(self.get_json(response.text))
        yield item

    def errback(self, failure):
        request = failure.request

        if failure.check(HttpError):
            response = failure.value.response
            self.logger.error(
                'errback <%s> %s , response status:%s' %
                (request.url, failure.value, response_status_message(response.status))
            )
        elif failure.check(ResponseFailed):
            self.logger.error('errback <%s> ResponseFailed' % request.url)

        elif failure.check(ConnectionRefusedError):
            self.logger.error('errback <%s> ConnectionRefusedError' % request.url)

        elif failure.check(ResponseNeverReceived):
            self.logger.error('errback <%s> ResponseNeverReceived' % request.url)

        elif failure.check(TCPTimedOutError, TimeoutError):
            self.logger.error('errback <%s> TimeoutError' % request.url)

        else:
            self.logger.error('errback <%s> OtherError' % request.url)

    @staticmethod
    def get_json(text):
        mydict = lambda: defaultdict(mydict)
        dict = mydict()
        text = text.replace('"],0,7]', '')
        text = text.split('","')
        dict["vendor"] = text[text.index("Vendor") + 1]
        dict["scanner"] = text[text.index("Scanners") + 1]
        dict["affected_products"]["product"] = text[text.index("Product") + 1]
        dict["affected_products"]["category"] = text[text.index("Category") + 1]
        dict["affected_products"]["affected_versions"] = text[text.index("Affected Versions") + 1]
        dict["solutions"] = []

        try:
            dict1 = {}
            dict1["name"] = text[text.index("name") + 1]
            dict1["type"] = text[text.index("type") + 1]
            dict1["description"] = text[text.index("description") + 1]
            dict["solutions"].append(dict1)
            dict2 = {}
            dict2["name"] = text[text.index("solution") + 2]
            dict2["type"] = text[text.index("solution") + 4]
            dict2["description"] = text[text.index("solution") + 3]
            dict["solutions"].append(dict2)
            start = text.index("solution") + 6
            if start == len(text) - 1:
                dict3 = {}
                dict3["name"] = text[text.index("solution") + 2]
                dict3["type"] = text[text.index("solution") + 4]
                dict3["description"] = text[text.index("solution") + 6]
                dict["solutions"].append(dict3)
            else:
                for i in range(start, len(text) - 1, 2):
                    dict3 = {}
                    dict3["name"] = text[text.index("solution") + 2]
                    dict3["type"] = text[text.index("solution") + 4]
                    dict3["description"] = text[i]
                    dict["solutions"].append(dict3)
        except:
            pass
        return dict

    @staticmethod
    def get_headers():
        headers = {
            "authority": "www.vulnerabilitycenter.com",
            "sec-ch-ua": "\" Not A;Brand\";v=\"99\", \"Chromium\";v=\"90\", \"Google Chrome\";v=\"90\"",
            "x-gwt-module-base": "https://www.vulnerabilitycenter.com/svc/svc/",
            "x-gwt-permutation": "938A4411868A979D515A5CE34BBE59F8",
            "sec-ch-ua-mobile": "?0",
            "content-type": "text/x-gwt-rpc; charset=UTF-8",
            "accept": "*/*",
            "origin": "https://www.vulnerabilitycenter.com",
            "sec-fetch-site": "same-origin",
            "sec-fetch-mode": "cors",
            "sec-fetch-dest": "empty",
            "referer": "https://www.vulnerabilitycenter.com/svc/SVC.html",
            "accept-language": "en-US,en;q=0.9"
        }
        return headers

    @staticmethod
    def get_cookies():
        cookie_string = "JSESSIONID=013D93A42C13C8C394BB6E23AFD58AB0; SESS9ab004767f41bf8781a9a80db17b50c6=" \
                        "ioestvbm99op2bihm5rndso7i5; has_js=1; d-a8e6=7a57e4df-2d16-49b3-8505-c3c4a4fbfe4e; " \
                        "__utmc=66189737; _mkto_trk=id:440-MPQ-510&token:_mch-vulnerabilitycenter.com" \
                        "-1626233949740-15893; __adroll_fpc=8bee9b06fa2a2428e5b39d6ecafeaf52-1626233951918; " \
                        "_fbp=fb.1.1626233953278.1377671768; __utmz=66189737.1626361011.9.4.utmcsr=google|utmccn=" \
                        "(organic)|utmcmd=organic|utmctr=(not%20provided); OptanonAlertBoxClosed=2021-07-15T17:" \
                        "05:51.065Z; s-9da4=6ff63c2b-4672-4935-8f7e-bdc44ffde8c1; __utma=66189737.1200352611.1626233" \
                        "950.1626366486.1626374352.11; __ar_v4=A24BPKS7XRBSZPUWDMSPMD%3A20210713%3A41%7C4DUFBJ677FAQ" \
                        "NKPWAVW5TK%3A20210713%3A41%7CW5L5PZHOHNFQTHBFOIEKOW%3A20210713%3A41; OptanonConsent=landing" \
                        "Path=NotLandingPage&datestamp=Fri+Jul+16+2021+01%3A50%3A38+GMT%2B0700+(Indochina+Time)&" \
                        "version=3.6.28&groups=1%3A1%2C2%3A1%2C3%3A1%2C4%3A1%2C0_162101%3A1%2C0_162100%3A1%2C0_162" \
                        "099%3A1%2C0_162105%3A1%2C0_162104%3A1%2C0_162103%3A1%2C0_162102%3A1%2C0_162109%3A1%2C0_16" \
                        "2108%3A1%2C0_162107%3A1%2C0_162106%3A1%2C0_162113%3A1%2C0_162112%3A1%2C0_162111%3A1%2C0_162" \
                        "110%3A1&AwaitingReconsent=false; __utmt=1; __utmb=66189737.5.10.1626374352"
        cookies = {}
        for cookie in cookie_string.split('; '):
            try:
                key = cookie.split('=')[0]
                val = cookie.split('=')[1]
                cookies[key] = val

            except:
                pass
        return cookies

    @staticmethod
    def get_body(id):
        body = "7|0|6|https://www.vulnerabilitycenter.com/svc/svc/|4EB3C5214EE901B9C18B44EBC7FF695D|" \
               "com.skybox.svc.shared.SVCService|getVulnerabilityRecord|java.lang.String/2004016611|{}|1|2|3|4|1|5|6|"
        return body.format(id)