# CRAWLING VUNERABILITY
This is a project for crawling all details about vunerabilities from <br>
https://nvd.nist.gov/vuln/full-listing <br>
https://www.vulnerabilitycenter.com

Author: **Trần Tuấn Ngọc** ([Fancol Tran](https://gitlab.com/ngoc_98))
## PYTHON VERSION
**3.8**
## DATABASE STRUCTURE
Database Management System: Mongodb Enterprise <br>
Database name: cveDB <br>
Collection name: cves <br>
```
{ "_id" : { "bsonType": "string" }, "year" : { "bsonType": "string" }, "month" : { "bsonType": "string" } }
```
Collection name: cvedetails <br>
``` 
"_id" : { "bsonType": "string" },
"description" : { "bsonType": "string" },
"published_date" : { "bsonType": "string" },
"last_modified" : { "bsonType": "string" },
"source" : { "bsonType": "string" },
"severity" : {
	"cvss_ver_3x" : {
	    "base_score" : { "bsonType": "string" }
	},
	"cvss_ver_2" : {
	    "base_score" : { "bsonType": "string" }
	}
},
"skybox_id" : { "bsonType": "string" },
"vendor" : { "bsonType": "string" },
"scanner" : { "bsonType": "string" },
"affected_products" : {
    "product" : { "bsonType": "string" },
    "category" : { "bsonType": "string" },
    "affected_versions" : { "bsonType": "string" }
},
"solutions" : [
    {
        "name" : { "bsonType": "string" },
        "type" : { "bsonType": "string" },
        "description" : { "bsonType": "string" }
    },
]
}
```
## INSTALL
```
git clone https://gitlab.com/ngoc_98/crawl_7.14.git 
```
## RUNNING MONGODB
```
sudo systemctl start mongod
```
## SETTUP
```
pip install -r requirements.txt
```
### RUNNING
if your python executable is python
```
python main.py
```
if your python executable is python3
```
python3 main.py
```
